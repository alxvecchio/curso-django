from django.http import HttpResponse    #HttpResponse (envia la información/respuesta) y request (petición al server para que nos muestre la URL) son 2 clases de 
                                        #la biblioteca de django y estan dentro del módulo django.http 
                                          
def saludo(request):    #A los archivos "view" se los denomina "vista". Esta es nuestra 1er vista. En django creamos una vista declarando una función. 
                        #Esta función recibe un request y devuelve lo que necesitemos con el "return"

    return HttpResponse("Hola Alex Vecchio. Hoy es 07/06/2021")


def despedida(request):

    return HttpResponse("Hasta la vista baby")
